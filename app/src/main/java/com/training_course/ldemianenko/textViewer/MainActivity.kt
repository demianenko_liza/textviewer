package com.training_course.ldemianenko.textViewer

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.training_course.ldemianenko.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    /**variable reflecting whether the text is visible*/
    private var isShown = true

    /**launches the textViewer application*/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        userInterfaceInit()
    }

    /**Reloads data about scroll position and text visibility*/
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        isShown = savedInstanceState.getBoolean("isShown")
        textView_main_textFromFile.scrollY = savedInstanceState.getInt("position")
        setView()
    }

    /**Reads text from a file and reacts to actions with a button*/
    private fun userInterfaceInit() {
        textView_main_textFromFile.movementMethod = ScrollingMovementMethod()
        textView_main_textFromFile.text = readText()
        button.setOnClickListener {
            isShown = !isShown
            setView()
        }
    }

    /**Saves data about scroll position and text visibility*/
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("isShown", isShown)
        outState.putInt("position", textView_main_textFromFile.scrollY)
    }

    /**Reads text from a file*/
    private fun readText(): String {
        val filename: String = getString(R.string.filename)
        return applicationContext.assets.open(R.string.filename.toString()).bufferedReader().use {
            it.readText()
        }
    }

    /**Sets the appearance of the application depending on whether the button is pressed*/
    private fun setView() {
        if (isShown) {
            button.background = resources.getDrawable(R.drawable.button_style_red)
            button.text = resources.getText(R.string.button_hide)
            textView_main_textFromFile.visibility = View.VISIBLE
        } else {
            button.background = resources.getDrawable(R.drawable.button_style_green)
            button.text = resources.getText(R.string.button_show)
            textView_main_textFromFile.visibility = View.INVISIBLE
        }
    }
}
